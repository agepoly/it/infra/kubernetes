FROM debian:buster
MAINTAINER informatique@agepoly.ch
ARG version=0.4
ARG project_name="polybanking"

RUN echo "locales locales/locales_to_be_generated multiselect en_US.UTF-8 UTF-8" | debconf-set-selections

RUN apt-get update
RUN apt-get install -y locales
RUN locale-gen en_US.UTF-8

ENV DEBIAN_FRONTEND noninteractive
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US
ENV LC_ALL en_US.UTF-8

RUN apt-get update
RUN apt-get install -y \
    apache2 \
    git \
    libapache2-mod-wsgi \
    memcached \
    python-pip \
    python-memcache \
    python-mysqldb \
    python-crypto \
    python-pil \
    python-dev \
    python-typing \
    python-yaml \
    rabbitmq-server \
    s6

# Deploy apache conf
RUN a2dissite 000-default
COPY ./polybanking.apache /etc/apache2/sites-available/$project_name.conf
RUN a2ensite $project_name


# Git repo for build
ARG git_branch="devel"
ARG gitlab_token_name="gitlab+deploy-token-121207"
ARG gitlab_token_password="NMZcB1foxXez9exGAxpc"
ARG git_repo_path="agepoly/it/dev/polybanking"
ARG git_temp_path="/tmp/polybanking"
ARG pip_reqs=""
ARG www_root_path="/var/www/git-repo"

RUN mkdir -p $www_root_path
WORKDIR $www_root_path

# Uncomment build with remote repository
RUN git clone  --progress --verbose --single-branch  --branch $git_branch https://$gitlab_token_name:$gitlab_token_password@gitlab.com/$git_repo_path $project_name

# Uncomment for local build
#COPY ./ $www_root_path/polybanking/

RUN pip install -r /var/www/git-repo/polybanking/server/data/pip-reqs.txt

COPY ./s6 /lib/s6

RUN chown -R www-data:www-data /var/www/ 

EXPOSE 80

CMD "/lib/s6/init"
