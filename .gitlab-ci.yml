---
variables:
  DEBIAN_FRONTEND: noninteractive
  LANG: C.UTF-8
  LC_ALL: C.UTF-8
  CI_TOOLS_BUILDER_IMAGE: $CI_REGISTRY/$CI_PROJECT_PATH/tools/silhouette-builder
  CI_TOOLS_BUILDER_DOCKERFILE: ./scripts/Dockerfile-silhouette-builder
  CI_TOOLS_IMAGE: $CI_REGISTRY/$CI_PROJECT_PATH/tools/silhouette
  CI_TOOLS_DOCKERFILE: ./scripts/Dockerfile-silhouette
  GITLAB_URL: gitlab.com

stages:
  - prepare
  - generate k8s config
  - build
  - lock-images
  - deploy
  - test

Build tools:
  image: docker:latest
  stage: prepare
  services:
    - docker:dind
  before_script:
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
  script:
    - |
      # Preparing necessary docker images
      hashSilouetteSources() {
        find scripts/silhouette -type f -exec sha256sum '{}' \;
      }

      HASH_SILHOUETTE="$(hashSilouetteSources | md5sum | awk '{print $1}')"
      IMAGE_TAG="$(date +%Y%m)-$HASH_SILHOUETTE"

      previousBuilderImageNotFound() {
        echo "Could not find a previous builder. Using upstream haskell as cache. Brace for a long build..."
        docker pull haskell
        docker tag haskell "$CI_TOOLS_BUILDER_IMAGE"
        docker push "$CI_TOOLS_BUILDER_IMAGE"
      }

      toolsImageNotFound() {
        echo "Could not find the required tools image: $CI_TOOLS_IMAGE:$IMAGE_TAG. Building..."

        docker pull "$CI_TOOLS_BUILDER_IMAGE" || previousBuilderImageNotFound

        # The tools builder image is massive, it contains tons of source and compiled intermediary dependencies.
        # Use the last builder as cache. Stack is smart enough to figure out what needs to be built.
        docker build --build-arg=CACHE_IMAGE="$CI_TOOLS_BUILDER_IMAGE" -f "$CI_TOOLS_BUILDER_DOCKERFILE" . \
          -t "$CI_TOOLS_BUILDER_IMAGE"
        docker push "$CI_TOOLS_BUILDER_IMAGE"

        # Build the tools image. This essentially copies out silhouette from the builder image
        docker build --build-arg=BUILDER_IMAGE="$CI_TOOLS_BUILDER_IMAGE" -f "$CI_TOOLS_DOCKERFILE" . \
          -t "$CI_TOOLS_IMAGE:$IMAGE_TAG"
        docker push "$CI_TOOLS_IMAGE:$IMAGE_TAG"
      }

      docker pull "$CI_TOOLS_IMAGE:$IMAGE_TAG" || toolsImageNotFound

      docker tag "$CI_TOOLS_IMAGE:$IMAGE_TAG" "$CI_TOOLS_IMAGE:latest"
      docker push "$CI_TOOLS_IMAGE"

yamllint:
  image: debian:buster
  stage: prepare
  script:
    - apt-get update
    - apt-get -y install yamllint
    - yamllint AGEPoly

Generate distributions:
  image: $CI_TOOLS_IMAGE
  stage: generate k8s config
  script:
    - mkdir ./deployments
    # Generate images
    - silhouette image-list AGEPoly/1-simple-services.yml -o ./deployments/test-images.txt # Test images are not locked
    - silhouette image-list AGEPoly/1-simple-services.yml -o ./deployments/locked-images.txt
      --lock-images=$CI_REGISTRY/$CI_PROJECT_PATH --tag-images=$CI_COMMIT_SHORT_SHA
    # Generate test distribution
    - cp -r AGEPoly/0-custom-services/ ./deployments/test
    - silhouette secrets AGEPoly/1-simple-services.yml -o ./deployments/test/secrets.yml --from-examples
    - silhouette config test AGEPoly/1-simple-services.yml --destdir ./deployments/test --reqs
      --domain ageptest.ch
    # Generate staging distribution
    - cp -r AGEPoly/0-custom-services/ ./deployments/staging
      # Staging secrets are managed with ansible
    - silhouette config staging AGEPoly/1-simple-services.yml --destdir ./deployments/staging --reqs
      --domain agepoly.ch  --lock-images=$CI_REGISTRY/$CI_PROJECT_PATH --tag-images=$CI_COMMIT_SHORT_SHA
    # Generate production distribution
    - cp -r AGEPoly/0-custom-services/ ./deployments/production
      # Production secrets are managed with ansible
    - silhouette config production AGEPoly/1-simple-services.yml --destdir ./deployments/production --use-tld --reqs
      --domain agepoly.ch --lock-images=$CI_REGISTRY/$CI_PROJECT_PATH --tag-images=$CI_COMMIT_SHORT_SHA
  artifacts:
    paths:
      - ./deployments

build-images:
  image: docker:latest
  stage: build
  services:
    - docker:dind
  before_script:
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
  script:
    - ./scripts/imageBuilder.sh --registry-basepath
      $CI_REGISTRY/$CI_PROJECT_PATH --tag latest -s ./deployments/

lock-images:
  image: docker:latest
  stage: lock-images
  services:
    - docker:dind
  before_script:
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
  script:
    - ./scripts/imageLocker.sh -l ./deployments/locked-images.txt
  only:
    refs:
      - master

deploy-test:
  stage: deploy
  when: manual
  script:
  before_script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - ssh-add <(echo "$GIT_SSH_PRIV_KEY")
    - git config --global user.email "ci-bot@agepoly.ch"
    - git config --global user.name "test-deploy-bot"
    - mkdir -p ~/.ssh
    - ssh-keyscan $GITLAB_URL >> ~/.ssh/known_hosts

  script:
    # Push repo changes into current repo
    - git clone "git@gitlab.com:agepoly/it/infra/k8s_test_manifests.git" -n ./temp
    - cp -r ./deployments/test/ ./temp
    - cd ./temp
    - git add *
    - git commit -m "Deploy new version" --allow-empty
    - git push

  environment:
    name: test
    url: https://test.ageptest.ch

deploy-staging:
  stage: deploy
  only:
    refs:
      - master
  before_script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - ssh-add <(echo "$GIT_SSH_PRIV_KEY")
    - git config --global user.email "ci-bot@agepoly.ch"
    - git config --global user.name "test-deploy-bot"
    - mkdir -p ~/.ssh
    - ssh-keyscan $GITLAB_URL >> ~/.ssh/known_hosts
 
  script:
    # Push repo changes into current repo
    - git clone "git@gitlab.com:agepoly/it/infra/k8s_staging_manifests.git" -n ./temp
    - cp -r ./deployments/staging/ ./temp
    - cd ./temp
    - git add *
    - git commit -m "Deploy new version" --allow-empty
    - git push

  environment:
    name: staging
    url: staging.agepoly.ch

deploy-prod:
  stage: deploy
  only:
    - tags
    - /^prod-.*$/
  when: manual
  before_script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - ssh-add <(echo "$GIT_SSH_PRIV_KEY")
    - git config --global user.email "ci-bot@agepoly.ch"
    - git config --global user.name "test-deploy-bot"
    - mkdir -p ~/.ssh
    - ssh-keyscan $GITLAB_URL >> ~/.ssh/known_hosts
 
  script:
    # Push repo changes into current repo
    - git clone "git@gitlab.com:agepoly/it/infra/k8s_production_manifests.git" -n ./temp
    - cp -r ./deployments/production/ ./temp
    - cd ./temp
    - git add *
    - git commit -m "Deploy new version" --allow-empty
    - git push
  environment:
    name: production
    url: agepoly.ch

Integration tests:
  stage: test
  only:
    refs:
      - master
  script:
    - ./scripts/integrationTests.sh
