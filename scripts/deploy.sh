#!/usr/bin/env sh
#
# (C) AGEPoly (Association Générale des Etudiants de l’EPFL)
#     Téo Goddet, Roosembert Palacios, 2019
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# shellcheck disable=2164

set -e


# Exit codes
E_SUCCESS=0
E_USER=1
E_K8S=2
E_APPLY=3

die()
{
    retval="$1"; shift
    if [ $# -eq 0 ]; then
        cat <&0 >&2
    else
        printf "%s" "$@" >&2; echo >&2
    fi
    if [ "$retval" = $E_USER ]; then
        echo "Run with --help for more information." >&2
    fi
    exit "$retval"
}

BASENAME="$0"

usage()
{
  cat <<-EOF
	$BASENAME: Find and deploy kubernetes config files (adjusting the image tag if needed)

	Usage:
	$BASENAME [-h]

	$BASENAME --namespace namespace  -s SEARCH_PATH --kubeconfig kubeconfigpath

	Description:
	  This program find all files in SEARCH_PATH that are kubernetes config files
	  For each file, if REGISTRY_BASEPATH is given, it replace the image tag by REPLACE_TAG
	  Then it create the namespace NAMESPACE if not existant and deploy the image in this namespace
	  with kube config in KUBECONFIG_PATH


	Options
	  -h, --help
	      Show this help message and exit.
	  -n, --namespace
	      Namespace where the config is applied
	  -s, --search-path
	      Path to search for config files
	  --kubeconfig
		  kubeconfig where to deploy
	  --createnamespace
		  Create namespace if this flag is present and namespace not existent, 

	Example:
	  $BASENAME -n staging -s ./projects --kubeconfig ./config
	      This command will find all config files under ./config and apply them on the
		  kubernetes cluster configured in ./config

	EOF
}


while [ $# -gt 0 ]; do
    opt="$1"; shift
    case "$opt" in
        (-h|--help) usage; exit ;;
        (-s|--search-path) SEARCH_PATH="$1"; shift ;;
        (-n|--namespace) NAMESPACE="$1"; shift ;;
        (--kubeconfig) KUBECONFIG_PATH="$1"; shift ;;
        (--create-namespace) CREATE_NAMESPACE=1; shift ;;
        (-*) die $E_USER 'Unknown option: %s' "$opt" ;;
        (*) die $E_USER 'Trailing argument: %s' "$opt" ;;
    esac
done

prepare()
{
 echo "configuration of the kubectl environement"

 echo "Using kube config located at : $KUBECONFIG_PATH"
  export KUBECONFIG=$KUBECONFIG_PATH

  kubectl version

   if ! kubectl get pods -n "$NAMESPACE"; then
          die $E_K8S "Error when connecting to the cluster"
   fi

   if [ "${CREATE_NAMEPACE:-0}" = 1 ] && ! kubectl get namespaces | grep -q "^${NAMESPACE}[[:blank:]]"; then
        echo "namespace $NAMESPACE not found... creating it..."
        if !  kubectl create namespace "$NAMESPACE"; then
              die $E_K8S "Could not create namespace $NAMESPACE"
        fi
        echo "namespace $NAMESPACE created."
  fi

}

explore_project()
{
  CONFIG_PATH="$1"

  echo "Processing file $1"
  
  if ! kubectl apply -f "$CONFIG_PATH" -n "$NAMESPACE"; then
    die $E_APPLY "Could not apply $CONFIG_PATH to the cluster"
  fi
  
  # label freshly created/updated/reapplied ressources to avoid deletion 
  kubectl label -f "$CONFIG_PATH" -n "$NAMESPACE" gitlab-ci-to-delete=false --all --overwrite

}

WORKING_DIRECTORY="$(pwd)"

prepare

kubectl label deployment,services,ingressroutes,pvc -n "$NAMESPACE" gitlab-ci-to-delete=true --all --overwrite

echo "The following files will be applied on the cluster : "
find "$SEARCH_PATH" -type f  -name "*.yml" -o -name "*.yaml" -o -name "*.json"

find "$SEARCH_PATH" -type f -name "*.yml" -o -name "*.yaml" -o -name "*.json" | while read -r config_path; do
  PROJECT_DIRECTORY="${config_path%/*}"
  cd "$WORKING_DIRECTORY"/"$PROJECT_DIRECTORY" >/dev/null
  explore_project "./${config_path##*/}"
done

kubectl delete deployment,services,ingressroutes,pvc -n $NAMESPACE -l gitlab-ci-to-delete=true

exit $E_SUCCESS
