#!/usr/bin/env sh
#
# (C) AGEPoly (Association Générale des Etudiants de l’EPFL)
#     Roosembert Palacios, 2019
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# shellcheck disable=2164

set -e

# Exit codes
E_SUCCESS=0
E_USER=1

die()
{
    retval="$1"; shift
    if [ $# -eq 0 ]; then
        cat <&0 >&2
    else
        printf "%s" "$@" >&2; echo >&2
    fi
    if [ "$retval" = $E_USER ]; then
        echo "Run with --help for more information." >&2
    fi
    exit "$retval"
}

BASENAME="$0"

usage()
{
  cat <<-EOF
	$BASENAME: Pull and push docker images in a specified file.

	Usage:
	$BASENAME [-h]

	$BASENAME -l IMAGE_LIST.csv

	Description:
	  This program pull and push all images in the IMAGE_LIST.csv. Each line should
	  contain a name, a source image and a target image. The name (first column)
	  will be ignored. If the target image does not exists, the source image will
	  be retrieved, tagged as the target image and pushed.

	Options
	  -h, --help
	      Show this help message and exit.
	EOF
}

while [ $# -gt 0 ]; do
    opt="$1"; shift
    case "$opt" in
        (-h|--help) usage; exit ;;
        (-l|--list) IMAGE_LIST="$1"; shift ;;
        (-*) die $E_USER 'Unknown option: %s' "$opt" ;;
        (*) die $E_USER 'Trailing argument: %s' "$opt" ;;
    esac
done

if [ -z "$IMAGE_LIST" ]; then
  die $E_USER 'Missing required option.'
fi

IFS=","
while read -r _ SOURCE_IMAGE TARGET_IMAGE; do
  if ! docker pull "$TARGET_IMAGE" >/dev/null; then
    docker pull "$SOURCE_IMAGE"
    docker tag "$SOURCE_IMAGE" "$TARGET_IMAGE"
    docker push "$TARGET_IMAGE"
    docker image prune -a -f
  fi
done < "$IMAGE_LIST"

exit $E_SUCCESS
