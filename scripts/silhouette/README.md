# Silhouette

Silhouette transpiles simple application specifications into applicable
Kubernetes deployments.

An application specification is a declarative description of its requirements.
For example:

```yaml
- appName: paste
  owner: agepinfo
  port: 80
  imageName: 'privatebin/nginx-fpm-alpine'
  volumes:
    - name: private-data
      path: /srv/data
      size: 15G
```

This specification will be translated to a full-featured Kubernetes
configuration. Containing services, deployments, persistencevolumes,
ingressroutes, secrets and what not.

## Development

We use [The Haskell Tool Stack (stack)](https://docs.haskellstack.org/en/stable/README/)
to manage the project.

**How to run?**

```
$ stack run silhouette -- --help
```

** Please go check the package.yaml file now **

From the stack documentation:

> If you want to launch a REPL:
>
> ```
> stack ghci
> ```
