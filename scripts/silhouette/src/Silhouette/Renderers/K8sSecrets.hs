{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -Werror=missing-fields #-}

-- (C) AGEPoly (Association Générale des Etudiants de l’EPFL)
--     Roosembert Palacios, 2019
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- This file is part of Silhouette

module Silhouette.Renderers.K8sSecrets where

import Data.Text (Text)
import Silhouette.TemplateManagement (TemplateLib(..), renderTemplate)
import Silhouette.Types (AppSecrets(..))

import qualified Data.Text as T
import qualified Silhouette.TemplateManagement as TM

renderSecrets :: TemplateLib -> [AppSecrets] -> Text
renderSecrets TemplateLib{..} secretConfigs = T.unlines renderedSecrets
  where renderedSecrets = map render' . filter appHasSecrets $ secretConfigs
        render' AppSecrets{..} = T.unlines $
          map (renderTemplate tSecret) . filter hasSecretAtoms $ secrets
        appHasSecrets AppSecrets{..} = length secrets > 0
        hasSecretAtoms TM.Secret{..} = length secrets > 0
