{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

-- (C) AGEPoly (Association Générale des Etudiants de l’EPFL)
--     Roosembert Palacios, 2019
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- This file is part of Silhouette

module Silhouette.Readers.ApplicationSpec where

import Control.Applicative
import Control.Monad
import Data.Aeson
import Data.Aeson.Types (Parser)
import Data.Aeson.TH (deriveJSON, deriveToJSON, defaultOptions, Options(..))
import Data.Char (isDigit)
import Data.Data (Data)
import Data.HashMap.Strict (member)
import Data.Maybe (fromMaybe)
import Data.Text (Text)
import GHC.Natural
import System.IO

import qualified Data.Text as T

failIfNewline :: Text -> Text -> Parser Text
failIfNewline propertyName line = do
  let fail' = fail (T.unpack propertyName ++ " must be a single line.")
  unless (T.all ('\n' /=) line) fail'
  return line

attrLine :: Object -> Text -> Parser Text
attrLine v p = v .: p >>= (failIfNewline $ "Attribute " <> p)

-- | Extract an optional attribute from a JSON object, if the atribute exists,
-- | it must not contain any newline characters.
attrLineWithDefault :: Object -> Text -> Text -> Parser Text
attrLineWithDefault v p def =
  v .:? p .!= def >>= (failIfNewline $ "Attribute " <> p)

-- | Extract an optional attribute from a JSON object, if the atribute exists,
-- | none of its elements must contain any newline characters.
attrLineWithDefault' ::
  (Traversable t, FromJSON (t Text)) => Object -> Text -> t Text -> Parser (t Text)
attrLineWithDefault' v p def =
  v .:? p .!= def >>= mapM (failIfNewline $ "Elements of " <> p)

newtype VolumeSize = VolumeSize Text deriving (Show, Eq)

instance FromJSON VolumeSize where
  parseJSON = withText "Volume size" $ (VolumeSize <$>) . mfilter (T.all isValidChar) . return
    where isValidChar c = isDigit c || c `elem` ("MG" :: String)

$(deriveToJSON defaultOptions ''VolumeSize)

data Volume = Volume
  { name :: Text
  , path :: FilePath
  , size :: VolumeSize
  } deriving (Show, Eq)

instance FromJSON Volume where
  parseJSON = withObject "Volume" $ \v -> do
    name <- attrLine v "name"
    path <- v .: "path"
    size <- v .: "size"
    return Volume{..}

$(deriveToJSON defaultOptions ''Volume)

data SecretEnvVar = SecretEnvVar
  { name :: Text
  , description :: Text
  , example :: Text
  } deriving (Show, Eq)

$(deriveToJSON defaultOptions ''SecretEnvVar)

instance FromJSON SecretEnvVar where
  parseJSON = withObject "SecretEnvVar" $ \v -> do
    name <- attrLine v "name"
    description <- v .: "description"
    example <- v .: "example"
    return SecretEnvVar{..}

data SecretFile =
    SecretFileInline
    { path :: FilePath
    , description :: Text
    , example :: Text
    , fileMode :: Maybe Natural
    }
  | SecretFileInFile
    { path :: FilePath
    , description :: Text
    , exampleFile :: Text
    , fileMode :: Maybe Natural
    }
  deriving (Show, Eq)

instance FromJSON SecretFile where
  parseJSON = withObject "Secret File" secret
    where secret v | member "example" v =
            SecretFileInline <$> v .: "path" <*> v .: "description" <*> v .: "example" <*> v .:?  "defaultMode" .!= Nothing
          secret v | member "exampleFile" v =
            SecretFileInFile <$> v .: "path" <*> v .: "description" <*> v .: "exampleFile" <*> v .:?  "defaultMode" .!= Nothing

class Secret a where
  secretName :: a -> Text

instance Secret SecretEnvVar where
  secretName SecretEnvVar{name} = name

instance Secret SecretFile where
  secretName SecretFileInline{path} = T.pack path
  secretName SecretFileInFile{path} = T.pack path

data AppSecrets = AppSecrets
  { environment :: [SecretEnvVar]
  , files :: [SecretFile]
  } deriving (Show, Eq)

instance FromJSON AppSecrets where
  parseJSON = withObject "AppSecrets" $ \v -> AppSecrets
    <$> v .:?  "environment".!= []
    <*> v .:?  "files"      .!= []

data EnvVar = EnvVar 
  { name :: Text
  , value :: Text
  } deriving (Show, Eq)

instance FromJSON EnvVar where
  parseJSON = withObject "EnvVar" $ \v -> do
    name <- attrLine v "name"
    value <- v .: "value"
    return EnvVar{..}

data Application = Application
  { appName :: Text
  , subdomain :: Text
  , owner :: Text
  , port :: Natural
  , imageName :: Text
  , envVars :: [EnvVar]
  , initCommand :: Maybe Text
  , initImageName :: Maybe Text
  , imagePullSecretName :: Maybe Text
  , volumes :: [Volume]
  , secrets :: Maybe AppSecrets
  , dnsAliases :: [Text]
  , probePath :: Maybe Text
  , needSSO :: Bool
  } deriving (Show, Eq)

instance FromJSON Application where
  parseJSON = withObject "Application" $ \v -> do
    let linePropWithDefault = attrLineWithDefault v
        linePropWithDefault' ::  -- Type annotation required to hint genericity
          (Traversable t, FromJSON (t Text)) => Text -> t Text -> Parser (t Text)
        linePropWithDefault' = attrLineWithDefault' v
        lineProp = attrLine v
    appName             <- lineProp "appName"
    subdomain           <- linePropWithDefault "subdomain" appName
    owner               <- lineProp "owner"
    port                <- v .:  "port"
    imageName           <- lineProp "imageName"
    envVars             <- v .:? "environment" .!= []
    initCommand         <- linePropWithDefault' "initCommand" Nothing
    initImageName       <- linePropWithDefault' "initImageName" Nothing
    imagePullSecretName <- linePropWithDefault' "imagePullSecretName" Nothing
    volumes             <- v .:? "volumes" .!= []
    secrets             <- v .:? "secrets" .!= Nothing
    dnsAliases          <- linePropWithDefault' "dnsAliases" []
    probePath           <- v .:? "probePath" .!= Nothing
    needSSO             <- v .:? "needSSO" .!= False
    return Application{..}
