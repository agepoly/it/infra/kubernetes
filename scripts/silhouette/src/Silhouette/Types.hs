{-# LANGUAGE TemplateHaskell #-}

-- (C) AGEPoly (Association Générale des Etudiants de l’EPFL)
--     Roosembert Palacios, 2019
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- This file is part of Silhouette

module Silhouette.Types where

import Data.Aeson
import Data.Aeson.TH (deriveToJSON, defaultOptions)
import Data.Text (Text)

import qualified Silhouette.TemplateManagement as TM

data AppConfiguration = AppConfiguration
  { deployment :: TM.Deployment
  , service :: TM.Service
  , ingressRoute :: [TM.IngressRoute]
  , certificate :: [TM.Certificate]
  , volumeClaims :: [TM.PersistentVolumeClaim]
  } deriving (Show, Eq)

data AppSecrets = AppSecrets
  { appName :: Text
  , secrets :: [TM.Secret]
  } deriving (Show, Eq)

$(deriveToJSON defaultOptions ''AppSecrets)
