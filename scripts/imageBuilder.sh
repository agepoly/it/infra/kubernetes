#!/usr/bin/env sh
#
# (C) AGEPoly (Association Générale des Etudiants de l’EPFL)
#     Roosembert Palacios, 2019
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# shellcheck disable=2164

set -e


# Exit codes
E_SUCCESS=0
E_USER=1
E_BUILD=2
E_NOCMD=127

die()
{
    retval="$1"; shift
    if [ $# -eq 0 ]; then
        cat <&0 >&2
    else
        printf "%s" "$@" >&2; echo >&2
    fi
    if [ "$retval" = $E_USER ]; then
        echo "Run with --help for more information." >&2
    fi
    exit "$retval"
}

BASENAME="$0"

usage()
{
  cat <<-EOF
	$BASENAME: Find, build and publish docker images on subfolders.

	Usage:
	$BASENAME [-h]

	$BASENAME [--registry-basepath imagepath] [--no-push] [--no-build] -s SEARCH_PATH

	$BASENAME [--list-projects] -s SEARCH_PATH

	Description:
	  This program find all files matching case-unsensitive 'dockerfile' under SEARCH_PATH
	  For each file, the parent directory is used as image name. Then it builds a tag containing
	  the file hash, the month and year and tries to pull it via "docker pull".
	  If the image is not found, it will be build and then pushed to the specified registry.

	Options
	  -h, --help
	      Show this help message and exit.
	  --registry-basepath
	      Basepath where to push the images (e.g. registry.gitlab.com/project/images)
	  --no-push
	      Do not push built images
	  --no-build
	      Do not build, exit non-zero if any image must be built
	  --tag tag
	      Add tag to the image
	  -s, --search-path
	      Path to search for dockerfiles
	  --list-projects
	      List projects containing a Dockerfile found under SEARCH_PATH

	Example:
	  $BASENAME --registry-basepath registry.gitlab.com/me/myprojects/images --tag latest -s ./projects
	      This command will find all dockerfiles under ./projects. For example, if ./projects/tetris/dockerfile
	      is found and has md5sum ffff on February 2019, it will try to fetch
	      "registry.gitlab.com/me/myprojects/images/tetris:201902-ffff".
	      If such image is not found, it will be build and push.
	      Finally it will tag "tetris:201902-ffff" as "tetris:latest" and push those tags to
	      "registry.gitlab.com/me/myprojects/images/tetris".

	  $BASENAME --no-build -s ./projects
	      This command will find all dockerfiles under ./projects. For example, if ./projects/tetris/dockerfile
	      is found and has md5sum ffff on February 2019, it will try to fetch
	      "registry.gitlab.com/me/myprojects/images/tetris:201902-ffff".
	      If such image is not found, it will not be build and a non-zero status code will be returned.

	EOF
}

PUSH="1"
BUILD="1"
LIST_ONLY="0"

while [ $# -gt 0 ]; do
    opt="$1"; shift
    case "$opt" in
        (-h|--help) usage; exit ;;
        (--list-projects) LIST_ONLY=1 ;;
        (--no-build) BUILD=0 ;;
        (--no-push) PUSH=0 ;;
        (--registry-basepath) REGISTRY_BASEPATH="$1"; shift ;;
        (-s|--search-path) SEARCH_PATH="$1"; shift ;;
        (--tag) EXTRA_TAG="$1"; shift ;;
        (-*) die $E_USER 'Unknown option: %s' "$opt" ;;
        (*) die $E_USER 'Trailing argument: %s' "$opt" ;;
    esac
done

DATE="$(date +%Y%m%d)";

if [ "$LIST_ONLY" -eq "0" ]; then
  if ! command -v "docker" >/dev/null 2>/dev/null; then
    die $E_NOCMD ""
  fi

  if [ -z "$SEARCH_PATH" ]; then
    die $E_USER "SEARCH_PATH not specified."
  fi
fi

explore_project()
{
  IMAGE_NAME="$1"
  DOCKERFILE_PATH="$2"

  echo -- "Processing project $1"

  if [ -z "$REGISTRY_BASEPATH" ]; then
    IMAGE="$IMAGE_NAME"
  else
    IMAGE="$REGISTRY_BASEPATH/$IMAGE_NAME";
  fi

  HASH="$(md5sum "$DOCKERFILE_PATH" | awk '{print $1}')";
  IMAGE_TAG="$DATE-$HASH";

  if ! docker pull "$IMAGE:$IMAGE_TAG" >/dev/null; then
    if [ "$BUILD" -eq "0" ]; then
      die $E_BUILD "Could not find Image $IMAGE_NAME:$IMAGE_TAG"
    fi
    echo "Couldn't find required image: $IMAGE:$IMAGE_TAG. building..."
    if ! docker build -t "$IMAGE:$IMAGE_TAG" -f "$DOCKERFILE_PATH" . ; then
      die $E_BUILD "Could not build image $IMAGE:$IMAGE_TAG"
    fi
    if [ -n "$REGISTRY_BASEPATH" ] && [ "$PUSH" -eq "1" ]; then
      if ! docker push "$IMAGE:$IMAGE_TAG"; then
        die $E_BUILD "Could not push image $IMAGE:$IMAGE_TAG"
      fi
    fi
  fi

  if [ -n "$EXTRA_TAG" ]; then
    echo "Tagging image $IMAGE:$IMAGE_TAG -> $IMAGE:$EXTRA_TAG"
    docker tag "$IMAGE:$IMAGE_TAG" "$IMAGE:$EXTRA_TAG";
    if [ -n "$REGISTRY_BASEPATH" ] && [ "$PUSH" -eq "1" ]; then
      if ! docker push "$IMAGE:$EXTRA_TAG"; then
        die $E_BUILD "Could not push image $IMAGE:$EXTRA_TAG"
      fi
    fi
  fi
}

WORKING_DIRECTORY="$(pwd)"

find "$SEARCH_PATH" -iname dockerfile | while read -r dockerfile_path; do
  PROJECT_DIRECTORY="${dockerfile_path%/*}"
  if [ "$LIST_ONLY" -eq "1" ]; then
    echo "$PROJECT_DIRECTORY"
  else
    cd "$WORKING_DIRECTORY" >/dev/null
    cd "$PROJECT_DIRECTORY" >/dev/null
    explore_project "${PROJECT_DIRECTORY##*/}" "./${dockerfile_path##*/}"
  fi
done

exit $E_SUCCESS
